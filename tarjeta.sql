drop database if exists banco;
create database banco;

\c banco

create table persona(
	dni integer,
	nombre varchar(64),
	apellido varchar(64),
	direccion varchar(64),
    telefono varchar(64)
);

create table compra(
	id_compra integer,
	monto integer,
	fecha date, 
    comercio varchar(64),
    codigo_postal varchar(64),
    numero_tarjeta varchar(64),
    estado varchar(64) 
);

create table tarjeta(
	numero_tarjeta integer,
	dni integer,
	fecha_vencimiento date,
	cod_seguridad integer,
	limite integer, 
	saldo integer,   
    estado varchar(64) 
);

create table factura(
	id_factura integer,
	id_compra integer,
    periodo varchar(64),  
    monto_total integer,
	numero_tarjeta integer,
    dni integer,
	fecha_vencimiento date
);

create table alerta(
    id_alerta integer,
    numero_tarjeta integer,
    descripcion varchar(64)
);

alter table persona add constraint dni_pk primary key (dni);
alter table compra add constraint compra_pk primary key (id_compra);
alter table compra add constraint tarjeta_fk foreign key (numero_tarjeta) references tarjeta;

alter table tarjeta add constraint tarjeta_pk primary key (numero_tarjeta);
alter table tarjeta add constraint dni_fk foreign key (dni) references persona;

alter table factura add constraint factura_pk primary key (id_factura));
alter table factura add constraint dni_fk foreign key (dni) references persona;
alter table factura add constraint compra_fk foreign key (id_compra) references compra;

alter table alerta add constraint alerta_pk primary key (id_alerta)
alter table alerta add constraint tarjta_fk foreign key (numero_tarjeta) references tarjeta;
